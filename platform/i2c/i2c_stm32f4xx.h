/*
 * Copyright (c) 2022 Talkweb Co., Ltd.
 *
 * HDF is dual licensed: you can use it either under the terms of
 * the GPL, or the BSD license, at your option.
 * See the LICENSE file in the root of this repository for complete details.
 */
#ifndef I2C_STM32F4XX_H
#define I2C_STM32F4XX_H
#include "osal_mutex.h"

typedef enum {
    I2C_HANDLE_NULL = 0,
    I2C_HANDLE_1 = 1,
    I2C_HANDLE_2 = 2,
    I2C_HANDLE_3 = 3,
    I2C_HANDLE_MAX = I2C_HANDLE_3
} I2C_HANDLE;

struct RealI2cResource {
    uint8_t busId;
    uint8_t dev_mode;
    uint32_t dev_addr;
    uint32_t speed;
    struct OsalMutex mutex;
};

#endif
